package com.example.todoList.controllerTest;

import com.example.todoList.entity.TodoItem;
import com.example.todoList.repository.TodoListJPARepository;
import com.example.todoList.service.dto.TodoItemRequest;
import com.example.todoList.service.mapper.TodoItemMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoListControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoListJPARepository todoListJPARepository;
    @BeforeEach
    void setUp() {
        todoListJPARepository.deleteAll();
    }
    @Test
    void should_find_all_todolist() throws Exception {
        TodoItemRequest todoItem = new TodoItemRequest("this is a test",false);
        TodoItem savedTodoItem = todoListJPARepository.save(TodoItemMapper.toEntity(todoItem));

        mockMvc.perform(get("/todolist"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].content").value(todoItem.getContent()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todoItem.isDone()));
    }
    @Test
    void should_find_todo_list_by_id() throws Exception {
        TodoItemRequest todoItem = new TodoItemRequest("this is a test",false);
        TodoItem savedTodoItem = todoListJPARepository.save(TodoItemMapper.toEntity(todoItem));
        mockMvc.perform(get("/todolist/{id}", savedTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(todoItem.getContent()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoItem.isDone()));

    }
    @Test
    void should_delete_todolist_item() throws Exception {
        TodoItemRequest todoItem = new TodoItemRequest("this is a test",false);
        TodoItem savedTodoItem = todoListJPARepository.save(TodoItemMapper.toEntity(todoItem));

        mockMvc.perform(delete("/todolist/{id}", savedTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(todoListJPARepository.findById(savedTodoItem.getId()).isEmpty());
    }

    @Test
    void should_create_todolist_item() throws Exception {
        TodoItemRequest todoItem = new TodoItemRequest("this is a test",false);

        ObjectMapper objectMapper = new ObjectMapper();
        String todoItemRequest = objectMapper.writeValueAsString(todoItem);
        mockMvc.perform(post("/todolist")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoItemRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(result -> assertEquals(todoListJPARepository.count(),1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(todoItem.getContent()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
    }
    @Test
    void should_find_todo_item_by_done() throws Exception {
        TodoItemRequest todoItem = new TodoItemRequest("this is a test",true);
        TodoItem savedTodoItem = todoListJPARepository.save(TodoItemMapper.toEntity(todoItem));
        mockMvc.perform(get("/todolist?done={0}", true))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(savedTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].content").value(savedTodoItem.getContent()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(true));
    }
    @Test
    void should_update_todo_item_content_and_done() throws Exception {
        TodoItemRequest todoItem = new TodoItemRequest("this is a test",false);
        TodoItem savedTodoItem = todoListJPARepository.save(TodoItemMapper.toEntity(todoItem));
        TodoItemRequest updateTodoItem = new TodoItemRequest("this is a new test",true);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(updateTodoItem);
        mockMvc.perform(put("/todolist/{id}", savedTodoItem.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedTodoItem.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.content").value(updateTodoItem.getContent()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(updateTodoItem.isDone()));
    }

}
