package com.example.todoList.controller;

import com.example.todoList.entity.TodoItem;
import com.example.todoList.service.TodoListService;
import com.example.todoList.service.dto.TodoItemRequest;
import com.example.todoList.service.dto.TodoItemResponse;
import com.example.todoList.service.mapper.TodoItemMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
@RequestMapping("todolist")
public class TodoListController {
    private final TodoListService todoListService;

    public TodoListController(TodoListService todoListService) {
        this.todoListService = todoListService;
    }
    @GetMapping
    public List<TodoItemResponse> getAllTodoItem() {
        return todoListService.findAll();
    }
    @GetMapping("/{id}")
    public TodoItemResponse getTodoItemById(@PathVariable Long id) {
        return todoListService.findById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodoItem(@PathVariable Long id) {
        todoListService.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoItemResponse addTodoItem(@RequestBody TodoItemRequest todoitem) {
        return  todoListService.create(todoitem) ;
    }

    @GetMapping(params = "done")
    public List<TodoItemResponse> getTodoListByDone(@RequestParam boolean done) {
        return todoListService.findAllByDone(done);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TodoItemResponse updateTodoItem(@PathVariable Long id, @RequestBody TodoItemRequest todoItemRequest) {
        return todoListService.updateTodoItem(id,todoItemRequest);
    }
}
