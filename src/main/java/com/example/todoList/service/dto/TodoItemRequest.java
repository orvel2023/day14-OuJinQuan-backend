package com.example.todoList.service.dto;

public class TodoItemRequest {
    private String content;
    private boolean done;
    public TodoItemRequest(){}

    public TodoItemRequest(String content, boolean done) {
        this.content = content;
        this.done = done;
    }

    public String getContent() {
        return content;
    }

    public boolean isDone() {
        return done;
    }
}
