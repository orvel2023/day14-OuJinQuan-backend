package com.example.todoList.service;

import com.example.todoList.entity.TodoItem;
import com.example.todoList.exception.TodoListItemNotFoundException;
import com.example.todoList.repository.TodoListJPARepository;
import com.example.todoList.service.dto.TodoItemRequest;
import com.example.todoList.service.dto.TodoItemResponse;
import com.example.todoList.service.mapper.TodoItemMapper;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TodoListService {
    private final TodoListJPARepository todoListJPARepository;

    public TodoListService(TodoListJPARepository todoListJPARepository) {
        this.todoListJPARepository = todoListJPARepository;
    }
    public List<TodoItemResponse> findAll() {
        return todoListJPARepository.findAll().stream().map(TodoItemMapper::toResponse).collect(Collectors.toList());
    }
    public TodoItemResponse findById(Long id) {
        return TodoItemMapper.toResponse(todoListJPARepository.findById(id).orElseThrow(TodoListItemNotFoundException::new)) ;
    }
    public List<TodoItemResponse> findAllByDone(boolean done) {
        return todoListJPARepository.findByDone(done).stream().map(TodoItemMapper::toResponse).collect(Collectors.toList());
    }
    public TodoItemResponse create(TodoItemRequest todoItem) {
        return TodoItemMapper.toResponse(todoListJPARepository.save(TodoItemMapper.toEntity(todoItem))) ;
    }
    public void delete(Long id) {
        todoListJPARepository.deleteById(id);
    }
    public TodoItemResponse updateTodoItem(Long id, TodoItemRequest todoItem) {
        TodoItem todoItem1 = todoListJPARepository.findById(id).orElseThrow(TodoListItemNotFoundException::new);
        todoItem1.setContent(todoItem.getContent());
        todoItem1.setDone(todoItem.isDone());
        todoListJPARepository.save(todoItem1);
        return TodoItemMapper.toResponse(todoItem1);
    }


}
