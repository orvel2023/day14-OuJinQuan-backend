package com.example.todoList.service.mapper;

import com.example.todoList.entity.TodoItem;
import com.example.todoList.service.dto.TodoItemRequest;
import com.example.todoList.service.dto.TodoItemResponse;
import org.springframework.beans.BeanUtils;

public class TodoItemMapper {
    public TodoItemMapper() {
    }
    public  static TodoItem toEntity(TodoItemRequest request){
        TodoItem todoItem = new TodoItem();
        BeanUtils.copyProperties(request, todoItem);
        return  todoItem;
    }
    public static TodoItemResponse toResponse(TodoItem todoItem){
        TodoItemResponse todoItemResponse = new TodoItemResponse();
        BeanUtils.copyProperties(todoItem,todoItemResponse);
        return todoItemResponse;
    }

}
