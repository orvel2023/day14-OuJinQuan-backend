CREATE TABLE if NOT EXISTS todo (
    ID BIGINT NOT NULL AUTO_INCREMENT primary key,
    content VARCHAR(255) not null,
    done boolean
);